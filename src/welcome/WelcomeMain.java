package welcome;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class WelcomeMain extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("WelcomeWindow.fxml"));
        primaryStage.setTitle("RSChat - Welcome");
        Scene sc = new Scene(root, 300, 180);
        sc.getStylesheets().add("welcome_style.css");
        primaryStage.setScene(sc);
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> System.out.println("Stage is closing"));
    }

    public static void main(String[] args) throws Exception {
        launch(args);
    }
}
