package server;

import java.io.*;
import java.net.Socket;
import java.util.*;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class ChatHandler implements Runnable {
    private Socket socket;
    private DataInputStream inStream;
    private DataOutputStream outStream;
    private boolean isOn;
    private String name;

    private static List<ChatHandler> handlers = Collections.synchronizedList(new ArrayList<>());

    public ChatHandler(Socket socket, String sKey, String cipherMedium) throws IOException {
        this.socket = socket;
        inStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
        outStream = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
        outStream.writeUTF(sKey);
        outStream.flush();
        outStream.writeUTF(cipherMedium);
        outStream.flush();
        name = inStream.readUTF();
    }

    @Override
    public void run() {
        isOn = true;
        try {
            handlers.add(this);
            Iterator<ChatHandler> it = handlers.iterator();
            while (it.hasNext()) {
                ChatHandler c = it.next();
                if (c == this) {
                    break;
                }
                if (name.equals(c.name)) {
                    outStream.writeUTF("-exception");
                    throw new WrongNameException();
                } else {
                    outStream.writeUTF("-addname " + c.name);
                }
            }
            broadcast("-addname " + name);
            while (isOn) {
                String msg = inStream.readUTF();
                messageHandler(msg, socket);
            }
        } catch (WrongNameException ex) {
            handlers.remove(this);
            System.out.println("Закрыто соединение с " + socket.getInetAddress());
        } catch (Exception ex) {
            // При разрыве соединения удаляем пользователя и рассылаем всем об этом уведомления
            handlers.remove(this);
            broadcast("-delname " + name);
            System.out.println("Закрыто соединение с " + socket.getInetAddress());
        } finally {
            try {
                outStream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            try {
                socket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private synchronized static void messageHandler(String msg, Socket socket) throws IOException {
        broadcast(msg);
        if (msg.substring(0, 4).equals("file")) {
            // Сообщение перед файлом делится на 3 части, которые разделены @@@.
            // 1 часть - метаданные (file), 2 часть - размер файла, 3 часть - само сообщение
            long fileSize = Long.parseLong(msg.split("@@@")[1]);
            byte[] bFile = new byte[2048];
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            do {
                int a = socket.getInputStream().read(bFile);
                bo.write(bFile, 0, a);
                fileSize -= a;
            } while (fileSize > 0);
            broadcast(bo);
        }
    }

    private static void broadcast(ByteArrayOutputStream bo) {
        byte[] bFile = bo.toByteArray();
        handlers.forEach(c -> {
            try {
                c.socket.getOutputStream().write(bFile);
            } catch (IOException ex) {
                ex.printStackTrace();
                c.isOn = false;
            }
        });
    }

    private static void broadcast(String message) {
        handlers.forEach(c -> {
            try {
                c.outStream.writeUTF(message);
                c.outStream.flush();
            } catch (IOException ex) {
                ex.printStackTrace();
                c.isOn = false;
            }
        });
    }
}
