package welcome;

import client.ChatClient;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class WelcomeController {
    @FXML private TextField name;
    @FXML private TextField inetAddress;
    @FXML private TextField port;

    @FXML
    public void clicked(ActionEvent event) {
        try {
            new ChatClient(new Stage(), name.getText(), inetAddress.getText(), Integer.parseInt(port.getText()));
            Stage stage = (Stage) name.getScene().getWindow();
            stage.close();
        } catch (Exception ex) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Ошибка подключения");
            a.setHeaderText("Ошибка подключения к серверу");
            a.setContentText("Проверьте правильность введённых данных или обратитесь к администратору сервера");
            a.showAndWait();
        }
    }
}
