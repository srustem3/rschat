package client;

import cipher.Cipher;
import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import welcome.WelcomeMain;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class ChatClient implements Runnable {
    private Socket socket;
    private DataInputStream inStream;
    private DataOutputStream outStream;
    private boolean isOn;
    private static ChatClient chatClient;
    private StringBuilder chatContent = new StringBuilder("");
    private String name;
    private Byte[] key;
    private Stage stage;
    private ObservableList<String> userList = FXCollections.observableArrayList();
    private Cipher cipher;

    public Stage getStage() {
        return stage;
    }

    public static ChatClient getChatClient() {
        return chatClient;
    }

    public void run() {
        isOn = true;
        try {
            // Получаем ключ от сервера
            String key = inStream.readUTF();
            Byte[] bKey = new Byte[key.length()];
            for (int i = 0; i < bKey.length; i++) {
                bKey[i] = (byte)(key.charAt(i) - 48);
            }
            this.key = bKey;
            // Получаем от сервера способ шифрования
            String medium = inStream.readUTF();
            cipher = (Cipher) Class.forName("cipher." + medium).newInstance();

            while (isOn) {
                String line = inStream.readUTF();
                Controller.switchSendingBlock();
                if (line.contains("-addname ")) {
                    String hLine = line; // Т.к. переменная в конструкции ниже должна быть final
                    Platform.runLater(() -> userList.add(hLine.substring(9)));
                } else if (line.contains("-delname ")) {
                    String hLine = line; // Т.к. переменная в конструкции ниже должна быть final
                    Platform.runLater(() -> userList.remove(hLine.substring(9)));
                } else if (line.contains("-exception")) {
                    throw new WrongNameException();
                } else {
                    boolean fileNotification = false;
                    String fileName;
                    long fileSize = 0;
                    if (line.substring(0, 4).equals("file")) {
                        // Сообщение перед файлом делится на 3 части, которые разделены @@@.
                        // 1 часть - метаданные (file), 2 часть - размер файла, 3 часть - само сообщение
                        String[] lineArray = line.split("@@@");
                        fileSize = Long.parseLong(lineArray[1]);
                        line = lineArray[2];
                        fileNotification = true;
                    } else {
                        line = line.substring(4);
                    }

                    String decryptedMessage = cipher.decode(line, this.key);

                    boolean isUser = false; // Послал ли сам пользователь это сообщение или это был кто-то другое
                    String nameWithHtml = "<b>" + name + "</b>";
                    if (nameWithHtml.equals(decryptedMessage.split(":")[0])) {
                        isUser = true;
                    }

                    refreshData(decryptedMessage, isUser);

                    if (fileNotification) {
                        fileName = decryptedMessage.split("</font>")[1];
                        fileName = fileName.substring(0, fileName.indexOf("</div>")); // Т.к. концовка </div>

                        byte[] bFile = new byte[2048];
                        ByteOutputStream bo = new ByteOutputStream();
                        System.out.println("Ожидается данных: " + fileSize);
                        do {
                            int a = socket.getInputStream().read(bFile);
                            bo.write(bFile, 0 , a);
                            fileSize -= a;
                        } while (fileSize > 0);
                        Files.write(Paths.get("files/" + fileName), bo.getBytes(), CREATE, WRITE);
                        File file = new File("files/" + fileName);
                        if (fileName.contains(".jpg")) {
                            refreshData("<img style=\"max-height: 100px;\" src=\"file:///" + file.getAbsolutePath() + "\">", isUser);
                        }
                    }
                }
                Controller.switchSendingBlock();
            }
        } catch (WrongNameException ex) {
            showErrorMessage("Существующее имя", "Такое имя уже существует",
                    "Пожалуйста, выберите другое имя, такое уже существует в чате");
            System.out.println("Соединение с сервером закрыто");
        } catch (IOException ex) {
            System.out.println("Соединение с сервером закрыто");
        } catch (Exception ex) {
            showErrorMessage("Отсутствие класса для расшифровки",
                    "У вас отсутствует класс для расшифровки сообщений",
                    "Обратитесь к администратору чата для получения класса, способного расшифровывать сообщения в этом чате");
        }
    }

    public void refreshData(String msg, boolean isUserSent) {
        String position = isUserSent ? "right" : "left"; // Отправил ли юзер сообщение сам или его прислал кто-то другой
        chatContent.append("<div align=\"" + position + "\">" + msg + "</div>");
        chatContent.append("<br>");
        Platform.runLater(() -> Controller.setWebView(chatContent.toString()));
    }

    public void sendData(String msg, String begin) {
        try {
            String fullMessage = "<b>" + name + "</b>: <div class=\"messageDiv\">" + msg + "</div>";
            String encryptedMessage = cipher.encode(fullMessage, key);
            outStream.writeUTF(begin + encryptedMessage);
            outStream.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
            isOn = false;
        }
    }

    public void sendFile(File file) throws Exception {
        // Сообщение перед файлом делится на 3 части, которые разделены @@@.
        // 1 часть - метаданные (file), 2 часть - размер файла, 3 часть - само сообщение
        sendData("<font color=\"#A64B00\">Прислал(а) файл. Имя файла: </font>" + file.getName(), "file@@@" + file.length() + "@@@");
        System.out.println("Отправлено данных: " + file.length());
        byte[] bFile = Files.readAllBytes(file.toPath());
        socket.getOutputStream().write(bFile);
    }

    private void showErrorMessage(String title, String header, String text) {
        Platform.runLater(() -> {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle(title);
            a.setHeaderText(header);
            a.setContentText(text);
            a.showAndWait();
            Controller.close();
            try {
                new WelcomeMain().start(new Stage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public ChatClient(Stage primaryStage, String name, String inetAddress, int port) throws Exception {
        // Вставляем javascript, чтобы страничка автоматически прокручивалась вниз до конца
        chatContent.append("<head>");
        chatContent.append("   <script language=\"javascript\" type=\"text/javascript\">");
        chatContent.append("       function toBottom(){");
        chatContent.append("           window.scrollTo(0, document.body.scrollHeight);");
        chatContent.append("       }");
        chatContent.append("   </script>");
        chatContent.append("   <style type=\"text/css\">");
        chatContent.append("     .messageDiv {");
        chatContent.append("       background: #5ED2B8;");
        chatContent.append("       border-radius:6px;");
        chatContent.append("       width: 50%;");
        chatContent.append("       overflow:auto;");
        chatContent.append("       padding: 8px;");
        chatContent.append("     }");
        chatContent.append("   </style>");
        chatContent.append("</head>");
        chatContent.append("<body onload='toBottom()'>");
        this.name = name;
        stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("ChatWindow.fxml"));
        primaryStage.setTitle("RSChat - " + inetAddress + ":" + port + " - Имя: " + name);
        Scene sc = new Scene(root, 500, 800);
        sc.getStylesheets().add("chat_style.css");
        primaryStage.setScene(sc);
        primaryStage.show();
        // При закрытии страницы разрываем соединение
        primaryStage.setOnCloseRequest(event -> {
                isOn = false;
                try {
                    outStream.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                try {
                    socket.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });
        Controller.getController().getUserList().setItems(userList);
        Controller.setPromptText(name);
        try {
            File fileDirectory = new File("files");
            fileDirectory.mkdir(); // При необходимости создаётся директория, куда будут сохраняться присылаемые файлы
            socket = new Socket(inetAddress, port);
            inStream = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
            outStream = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
            chatClient = this;
            new Thread(this).start(); // Для параллельной работы с JavaFX
            outStream.writeUTF(name);
            outStream.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
            primaryStage.close();
            try {
                if (outStream != null) outStream.close();
            } catch (IOException ex2) {
                ex2.printStackTrace();
            }
            try {
                socket.close();
            } catch (IOException ex3) {
                ex3.printStackTrace();
            }
        }
    }
}
