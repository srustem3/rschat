package client;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class Controller {
    @FXML private WebView webView;
    @FXML private TextField textField;
    @FXML private ListView<String> userList;
    @FXML private Button sendMessage;
    @FXML private Button sendFile;
    private boolean sendingBlock = true;
    private static Controller c;

    public Controller() {
        c = this;
    }

    public ListView<String> getUserList() {
        return userList;
    }

    public static Controller getController() {
        return c;
    }

    public static void setPromptText(String name) {
        c.textField.setPromptText(name + ": ...");
    }

    public static void setWebView(String text) {
        c.webView.getEngine().loadContent(text);
    }

    @FXML
    public void clicked(ActionEvent event) {
        ChatClient cc = ChatClient.getChatClient();
        cc.sendData(textField.getText(), "mess");
        textField.setText("");
    }

    @FXML
    public void fileSelected(ActionEvent event) throws Exception {
        ChatClient cc = ChatClient.getChatClient();
        FileChooser fileChooser = new FileChooser(); // Класс работы с диалогом выборки и сохранения
        fileChooser.setTitle("Выбрать файл"); // Заголовок диалога
        File file = fileChooser.showOpenDialog(cc.getStage()); // Указываем текущую сцену
        if (file != null) {
            cc.sendFile(file);
        }
    }

    public static void switchSendingBlock() {
        c.sendingBlock = !c.sendingBlock;
        c.textField.setEditable(c.sendingBlock);
        c.sendMessage.setDisable(!c.sendingBlock);
        c.sendFile.setDisable(!c.sendingBlock);
    }

    public static void close() {
        Stage stage = (Stage) c.webView.getScene().getWindow();
        stage.close();
    }
}
