package cipher;

import java.util.*;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class Columns implements Cipher {
    public String decode(String source, Byte[] key) {
        List<List<Character>> columns = new ArrayList<>();
        for (int i = 0; i < key.length; i++) {
            columns.add(new ArrayList<>());
        }
        for (int i = 0; i < columns.size(); i++) {
            for (int j = (source.length() * (i)) / key.length; j < (source.length() * (i+1)) / key.length; j++) {
                if (source.charAt(j) == '*') {
                    columns.get(i).add(' ');
                } else {
                    columns.get(i).add(source.charAt(j));
                }
            }
        }

        List<List<Character>> columnsAfterKey = new ArrayList<>();
        for (int i = 0; i < key.length; i++) {
            columnsAfterKey.add(columns.get(key[i]));
        }

        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < columnsAfterKey.get(0).size(); i++) {
            for (int j = 0; j < key.length; j++) {
                if (columnsAfterKey.get(j).size() > i) {
                    sb.append(columnsAfterKey.get(j).get(i));
                }
            }
        }

        return sb.toString();
    }

    public String encode(String source, Byte[] key) {
        List<List<Character>> columns = new ArrayList<>();
        for (int i = 0; i < key.length; i++) {
            columns.add(new LinkedList<>());
        }
        for (int i = 0; i < source.length(); i++) {
            columns.get(i % key.length).add(source.charAt(i));
        }

        int q = columns.size() - 1;
        while (columns.get(q).size() < columns.get(0).size()) {
            columns.get(q).add('*');
            q--;
        }

        Map<Integer, List<Character>> map = new HashMap<>();
        for (int i = 0; i < key.length; i++) {
            map.put((int)key[i], columns.get(i));
        }

        StringBuilder res = new StringBuilder("");
        map.forEach((k, v) -> res.append(v.stream().map(i -> i + "").reduce("", (acc, el) -> acc + el)));
        // Reverse
        /*StringBuilder resReverse = new StringBuilder("");
        for (int i = 0; i < res.length(); i++) {
            resReverse.append(res.charAt(res.length() - i - 1));
        }*/
        return res.toString();
        // Обычная перестановка букв по ключу в пределах строк
        /*for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.get(i).getValue().size(); j++) {
                res.append(list.get(i).getValue().get(j));
            }
        }*/
    }
}