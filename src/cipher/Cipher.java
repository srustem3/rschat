package cipher;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public interface Cipher {
    String decode(String source, Byte[] key);
    String encode(String source, Byte[] key);
}