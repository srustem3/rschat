package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class ChatServer {
    public ChatServer(int port) throws IOException {
        ServerSocket service = new ServerSocket(port);
        // Генерируем ключ для шифрования сообщений
        ArrayList<Byte> key = new ArrayList<>();
        while(key.size() < 4) {
            byte b = (byte) (Math.random() * 4);
            if (!key.contains(b))
                key.add(b);
        }
        String sKey = key.stream()
                .map(b -> Byte.toString(b))
                .reduce("", (acc, el) -> acc + el);
        System.out.println("Сгенерированный ключ: " + sKey);
        System.out.println("Введите название класса: ");
        Scanner sc = new Scanner(System.in);
        String cipherMedium = sc.nextLine();
        System.out.println("Ок");
        ExecutorService es = Executors.newCachedThreadPool();
        try {
            while (true) {
                Socket s = service.accept();
                System.out.println("Установлено соединение с " + s.getInetAddress());
                ChatHandler handler = new ChatHandler(s, sKey, cipherMedium);
                es.execute(handler);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            es.shutdown();
            service.close();
        }
    }

    public static void main(String[] args) throws IOException {
        new ChatServer(8083);
    }
}
